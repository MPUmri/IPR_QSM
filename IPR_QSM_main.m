%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3D IPR-QSM and modified IPR-QSM                                         %
% Véronique Fortier                                                       %
% 2018                                                                    %
% Based on : Buch, S., Liu, S., Ye, Y., Cheng, Y. C. N., Neelavalli, J.,  %
% & Haacke, E. M. (2015). Susceptibility mapping of air, bone, and        %
% calcium in the head. Magnetic Resonance in Medicine, 73(6), 2185-2194.  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tic; clc; %clear all; %close all

filesPath_toAdd = strcat(pwd, '/Functions');
addpath(genpath(filesPath_toAdd));

% Define path for MRI data     
folder_MRI = '/Users/Desktop/Patient1_MR/odd';  


%% Define parameters for the algorithm

originalIPR = 0; % 0 to run modified IPR, 1 to run original IPR

% Define parameters for the IPR iterations
iterateNum = 0;           % Count for the total number of IPR iterations 
thresholdReplacement = 0.01;       % Convergence criteria on the root mean square error wih the previous iteration 
betaReplacement = 100*ones(1, 300);    % Initial matrix for root mean square error 

% Define maximum number of IPR iterations 
maxReplacementIteration = 5;

% Initiate variables
iterateReplacement = 0;
iterate = 0;

gamma = 267.513*10^6;     %rad/sT, for H


%% MRI dicom reading
% Read in imaging data from DICOM files
%
% Required data and information:
% Magnitude data, complex data
% TE, delta TE, voxel size, matrix size,
% central frequency, main field magnitude, and B0 direction 

% % Example for data acquired with a 3T Philips Ingenia
% % using two separate sequences (odd and even TEs):
% % this uses the function complexMultiEchoesDataReading() which is available in Functions/
% % The  data used for the original synthetic CT projected published in 2021 was acquired
% % with two sequences due to limitations on the echo spacing

% % read in the data for odd echoes
% folder_MRI = '/Users/Desktop/Patient1_MR/odd';
% filename_MRI = 'filename_MRI_odd.mat';
% sliceOrientation = 'sagittal'; % sagittal, coronal or axial
% complexMultiEchoesDataReading(folder_MRI, filename_MRI, sliceOrientation)

% % read in the data for even echoes
% folder_MRI = '/Users/Desktop/Patient1_MR/even';
% filename_MRI = 'filename_MRI_even.mat';
% sliceOrientation = 'sagittal'; % sagittal, coronal or axial
% complexMultiEchoesDataReading(folder_MRI, filename_MRI, sliceOrientation)


%% Load data
% Use this section of the code to load mat files created
% with the function complexMultiEchoesDataReading()
% Load odd echoes data first
% odd echoes are used for modified IPR-QSM processing
data=load('filename_MRI_odd'); 

% data classification
MagnData = double(data.dataScan.magnitudeReconstructed);
realIm = double(data.dataScan.real);
imIm = double(data.dataScan.im);

% collect the echo times
TE_all = data.dataScan.TE;

% compute the echo spacing
if length(TE_all) > 1
    delta_TE = TE_all(2) - TE_all(1);
else
    delta_TE = TE_all;
end

% reconstitute the complex data
iField = realIm + 1i*imIm;

% Gather additional parameters
B0_dir = data.dataScan.B0_dir';
voxelSize = data.dataScan.voxelSize;
matrixSize = data.dataScan.matrixSize;
CF = data.dataScan.CF;
mainField = 2*pi*CF / gamma; 


if originalIPR == 0
    % even echoes
    data = load('filename_MRI_even'); 

    % data classification
    MagnData2 = double(data.dataScan.magnitudeReconstructed);
    realIm2 = double(data.dataScan.real);
    imIm2 = double(data.dataScan.im);

    TE_all2 = data.dataScan.TE;
    iField2 = realIm2 + 1i*imIm2;
end

% clear memory
clearvars data realIm2 imIm2 realIm imIm


%% Combine odd and even data for monopolar readout 
% use this when the data acquisition is performed with two sequences

if originalIPR == 0
    index_odd = 0;
    % Combine data from odd and even echoes together
    for index = 1:length(TE_all)
        iField_flyback(:,:,:,index+index_odd) = iField(:,:,:,index);
        iField_flyback(:,:,:,index+index_odd+1) = iField2(:,:,:,index);    
        magn_flyback(:,:,:,index+index_odd) = MagnData(:,:,:,index);
        magn_flyback(:,:,:,index+index_odd+1) = MagnData2(:,:,:,index);  
        TE_flyback(i+index_odd) = TE_all(index);
        TE_flyback(i+index_odd+1) = TE_all2(index);
        index_odd = index_odd + 1;
    end

    % clear memory
    clearvars iField MagnData iField2 MagnData2    

    data_flyback = struct('TE', TE_flyback, 'iField', iField_flyback);
end


% Example function for masking 
[maskSoftTissue, maskObject, maskAirBone]=generate_masks(magn_flyback(:,:,:,2), matrixSize);  % Use the echo closest to the first in phase time for fat and water
maskAirBone = logical(maskAirBone);
maskSoftTissue = logical(maskSoftTissue); 
maskObject = logical(maskObject);

% Visualize the masking results
figure(); montage(reshape4montage(maskAirBone), 'DisplayRange',[0 1])
figure(); montage(reshape4montage(maskObject), 'DisplayRange',[0 1])     
figure(); montage(reshape4montage(maskSoftTissue), 'DisplayRange',[0 1])


%% Unwrapping the phase for original IPR technique only
if originalIPR == 1

    for indexEcho = 1:2
        if indexEcho == 1
            PhaseData_uw(:, :, :, indexEcho) = PhaseData(:, :, :, indexEcho);
        else
            PhaseData_uw(:, :, :, indexEcho) = PhaseData(:, :, :, indexEcho+1);
        end
    end

    % The quality-guided unwrapping technique can then be used to 
    % unwrapped PhaseData_uw: https://gitlab.com/veronique_fortier/Quality_guided_unwrapping 
    % The resulting 3D matrix should be named 'phaseModelUW'.
    
    % Example how to call the unwrapping function
    % phaseModelUW = qualityGuidedUnwrapping(PhaseData_uw, maskSoftTissue, qualityCutoff);

    % Phase offset must be removed. The technique presented in the following reference can be 
    % used to do this: Gilbert, G., Savard, G., Bard, C., & Beaudoin, G. (2012). 
    % Quantitative comparison between a multiecho sequence and a single-echo 
    % sequence for susceptibility-weighted phase imaging. Magnetic resonance 
    % imaging, 30(5), 722-730.  
    % The resulting 3D matrix should be named 'phaseModelUW1'.

    phaseModelUW1 = phaseModelUW1(:,:,:,1) .* maskSoftTissue;  

end


if originalIPR == 0

    % Fat water separation must be performed. The 3-point Dixon technique from the ISMRM
    % fat-water separation toolbox used in the publication
    % is available here: https://www.ismrm.org/workshops/FatWater12/data.htm
    % The resulting matrices for the fat and water complex signals should be respectively named
    % 'wfat' and 'wwater'
    %
    % Example how to call the 3-point Dixon from the ISMRM fat-water separation toolbox
    % algoParams.species(1).name = 'water';
    % algoParams.species(1).frequency = 4.70;

    % % Use a 9 peaks fat spectrum
    % algoParams.species(2).frequency = [0.90, 1.30, 1.60, 2.02, 2.24, 2.75, 4.20, 5.19, 5.29];
    % algoParams.species(2).relAmps = [88 642 58 62 58 6 39 10 37];
    % 
    %  % Threshold on magnitude weight for seed points
    % algoParams.c1 = 0.75; 
    % algoParams.c2 = 0.25; 
    % 
    % imDataParams.voxelSize=voxelSize;
    % imDataParams.FieldStrength=mainField;
    % imDataParams.PrecessionIsClockwise=1;
    % delta_TE=TE_flyback(2)-TE_flyback(1);
    % 
    % imDataParams.images=reshape((iField(:,:,:,1:3)),[matrixSize(1),matrixSize(2),matrixSize(3),1,3]); 
    % imDataParams.TE=TE_flyback(1:3);
    % 
    % outParams=fw_i3cm0i_3point_berglund( imDataParams, algoParams); 
    % wfreq=outParams.fieldmap; 
    % wfat=outParams.species(2).amps;
    % wwater=outParams.species(1).amps;
    % 
    % wfreq=(-wfreq*delta_TE*2*pi); %rescaling 


    % Calculate fat and water quantitative maps

    waterImageNorm = wwater;
    fatImageNorm = wfat;

    maskFat = zeros(matrixSize);
    maskFat(find(abs(fatImageNorm) > abs(waterImageNorm))) = 1;
    maskWater = zeros(matrixSize);
    maskWater(find(abs(waterImageNorm) >= abs(fatImageNorm))) = 1;
    maskFat = logical(maskFat);
    maskWater = logical(maskWater);
    maskFat = maskFat.*maskSoftTissue;
    maskWater = maskWater.*maskSoftTissue;

    [ f1, w1 ] = fatQuantification( wfat, wwater, matrixSize);

    % display the results
    figure(); montage(reshape4montage(w1), 'DisplayRange', [0 100]); %water fraction


    % A B0 field map corrected for the presence of chemical shift is needed to perform the IPR-QSM
    % The field map obtained as a result of the fat-water separation can be used for this
    % and must be spatially unwrapped.
    % The quality-guided unwrapping technique used in the
    % publication is available at: https://gitlab.com/veronique_fortier/Quality_guided_unwrapping 
    % The resulting 3D matrix should be named 'wfreqUW'.


    % Background field removal must be performed before performing IPR-QSM. A three level background removal technique based on 
    % the LBV algorithm is recommended. The recommended function for LBV 
    % background removal is available as part of the MEDI toolbox: 
    % http://pre.weill.cornell.edu/mri/pages/qsm.html

    % Input for the three-level background removal should be a B0 field map, i.e. the output
    % field map from fat-water separation after spatial unwrapping (wfreqUW)

    % Example of how to call the LBV() function from the MEDI toolbox using the
    % proposed three-level background removal approach:
    %
    % parameter1_forLBV = 0.0001 
    % parameter2_forLBV = 4 
    % parameter3_forLBV = 0 
    %
    % call LBV restricting the process to the full object, soft tissue, and water only
    % phase_airBone = LBV(wfreqUW, maskObject, matrixSize, voxelSize, parameter1_forLBV, parameter2_forLBV, parameter3_forLBV);
    % phase_fat = LBV(wfreqUW, maskSoftTissue, matrixSize, voxelSize, parameter1_forLBV, parameter2_forLBV, parameter3_forLBV); 
    % phase_water = LBV(wfreqUW, maskWater, matrixSize, voxelSize, parameter1_forLBV, parameter2_forLBV, parameter3_forLBV);

end

%% Define the forward and inverse filter in the Fourier domain with a threshold
[forwardFilter, inverseFilterReg] = defineFilters(B0_dir, matrixSize,voxelSize);


%% Initialize whole object phase and air+bone phase for IPR QSM 

phaseModel = phase_airBone(:,:,:,1).*maskObject;
phaseAirBoneMask = zeros(matrixSize);


%%  SWIM QSM 
if originalIPR == 0

    susceptibilityMap = (ifftn(ifftshift(inverseFilterReg.*fftshift(fftn(phase_fat)))) ./ (gamma*mainField*delta_TE).*10^6).*maskSoftTissue;                                                                    
    [iterateQSM_fat, iterateInverse] = inverseProcess(real(susceptibilityMap), maskSoftTissue, forwardFilter, matrixSize); 
    %figure(); imagesc((iterateQSM_fat(:,:,88)).*maskSoftTissue(:,:,88)); colormap gray; caxis([-1 1]); colorbar; axis off; set(gcf,'color','w'); set(gca, 'fontsize', 20, 'fontweight', 'bold');
                                                
    susceptibilityMap = (ifftn(ifftshift(inverseFilterReg.*fftshift(fftn(phase_water)))) ./ (gamma*mainField*delta_TE).*10^6).*maskSoftTissue;
    [iterateQSM_water, iterateInverse] = inverseProcess(real(susceptibilityMap), maskWater, forwardFilter, matrixSize);
    %figure(); imagesc((iterateQSM_water(:,:,88).*maskWater(:,:,88))); colormap gray; caxis([-1 1]); colorbar; axis off; set(gcf,'color','w'); set(gca,'fontsize', 20, 'fontweight', 'bold');

end


%%  IPR iterations
% The algorithm stops if the maximum number of iterations allowed is reached 
while iterateNum<maxReplacementIteration

    if iterate ~= 0
        iterateReplacement = iterateReplacement + 1
    end
    
    phaseImage = phaseModel.*maskSoftTissue + real(phaseAirBoneMask);     
        
    % Inverse equation 
    susceptibilityMap = (ifftn(ifftshift(inverseFilterReg.*fftshift(fftn(phaseImage)))) ./ (gamma*mainField*delta_TE).*10^6).*maskObject;
        
    [iterateQSM, iterateInverse] = inverseProcess(real(susceptibilityMap), maskAirBone, forwardFilter, matrixSize);
        
    iterateQSM = iterateQSM.*maskObject;     

    susceptibilityAirBone = iterateQSM.*maskAirBone; 

    
    % Calculate the phase induced by air+bone
    phaseAirBoneMask = (gamma*mainField*delta_TE*ifftn(ifftshift(fftshift(fftn(susceptibilityAirBone.*10^(-6))).*forwardFilter))).*maskAirBone;

    % ______________________________________________________________________________________________
    % Phase replacement
            
    if iterateReplacement ~= 0 
        % Calculate the RMSE with the previous iteration
        sommeReplace = sum(sum(sum((real(previousSusceptibility(maskObject)) - real(iterateQSM(maskObject))).^2)));
        betaReplacement(iterateReplacement+2) = sqrt(sommeReplace / sum(maskObject(:)));

        % Stop algorithm if RMS is smaller than the set threshold, or if
        % the algorithm starts diverging
        if (betaReplacement(iterateReplacement + 2) > betaReplacement(iterateReplacement + 1) || betaReplacement(iterateReplacement + 2) <= thresholdReplacement)
            if betaReplacement(iterateReplacement + 2) > betaReplacement(iterateReplacement + 1)
               iterateQSM = previousSusceptibility;   
            end

            iterate = maxReplacementIteration - 1;          
        end
    end    
    
    previousSusceptibility = iterateQSM;  
      
    iterate = iterate + 1;
    iterateNum = iterateNum + 1;
end

% Display the results of IPR QSM
figure(); montage(reshape4montage(permute(iterateQSM(:,:,:,1).*maskObject(:,:,:),[1,2,3])), 'DisplayRange', [-1 1])


%% Modified IPR-QSM 
if originalIPR == 0
    finalQSM = (iterateQSM_fat.*maskFat+iterateQSM_water.*maskWater + iterateQSM.*maskAirBone);
    finalQSM = finalQSM.*maskObject;
    figure(); montage(reshape4montage(permute(finalQSM(:,:,:,1), [1,2,3])), 'DisplayRange', [-1 1])

else
    finalQSM =  iterateQSM.*maskObject;

end


%% Reference for QSM

sliceRef = 30; % select a slice where neck muscle can be visualized
figure(); imagesc(MagnData(:, :, sliceRef)); colormap gray; % place a ROI in neck muscle, avoid streaking as much as possible

roiMuscle = roipoly;
QSM_ref = finalQSM(:, :, sliceRef);
ref_QSM = mean(QSM_ref(roiMuscle));

referencedQSM = finalQSM - ref_QSM;



