%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate masks for air and bone regions, for soft tissue regions and for 
% the whole object 
% Veronique Fortier                                                       %
% 2016                                                                    %
% ----------------------------------------------------------------------- %
% Input argument:
%   -magn_flyback (magnitude data at a short echo time to minimize
%   distorsion and signal loss, with monopolar readout)
%   -matrixSize
% 
% Output argument:
%   -maskSoftTissue is a mask of the soft tissue, exlucing bone
%   -maskObject is a mask of the whole object
%   -maskAirBone is a mask of the air and bone regions
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ maskSoftTissue, maskObject, maskAirBone] = generate_masks(magn_flyback, matrixSize)
    
    %% Histogram of all the pixels
    figure()
    h = histogram(magn_flyback(:,:,:,1)); 
    values = h.Values;
    values = values(values ~= 0); 

    vvalues = smooth(values);
    peaks = findpeaks(vvalues);
    peaks = max(peaks);

    indice_second_max = find(vvalues == peaks);
    vvalues = vvalues(1:indice_second_max);

    indice_min_Histo = find(vvalues == min(vvalues));
    if length(indice_min_Histo) > 1
        indice_min_Histo = indice_min_Histo(1);
    end

    width = h.BinWidth; 

    % background threshold
    background_threshold = indice_min_Histo*width;

    % Define the soft tissue mask
    maskSoftTissue = zeros(matrixSize);
    maskSoftTissue(find(magn_flyback(:,:,:,1) > background_threshold)) = 1; 

    % Define the contour mask by filling the soft tissue mask (3D)
    maskObject = maskSoftTissue;
    maskObject = imfill(maskObject, 'holes');

    %Morphological operations to ensure filling all air cavities 
    se = strel('diamond',15);
    maskObject = imdilate(maskObject, se);
    maskObject = imfill(maskObject, 'holes');
    se = strel('disk', 16);
    maskObject = imerode(maskObject, se);
    maskObject = imfill(maskObject, 'holes');

    %Define the air+bone mask
    maskAirBone = (maskObject - maskSoftTissue);
    maskObject(find(maskAirBone ==-1 )) = 1;
    maskAirBone = (maskObject - maskSoftTissue);

    %Remove small 'noise' regions from the mask with bwareaopen 
    maskAirBone = bwareaopen(maskAirBone, 20);
    maskObject = bwareaopen(maskObject, 26); 

    %Re-define mask soft tissue
    maskSoftTissue = (maskObject - maskAirBone);

    %Remove the contour. This is required for the unwrapping step with the
    %quality-guided unwrapping technique used in the publication
    maskObject(1,:,:) = 0;
    maskObject(:,1,:) = 0;
    maskObject(end,:,:) = 0;
    maskObject(:,end,:) = 0;

    maskAirBone = maskAirBone.*maskObject; 
    maskSoftTissue = maskSoftTissue.*maskObject;

end
