%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fat quantification                                                      %
% V�ronique Fortier                                                       %
% 2018                                                                    %
% Based on: Berglund J, Johansson L, Ahlstr�m H, Kullberg J. Three-point  %
% Dixon method enables whole-body water and fat imaging of obese subjects.%
% Magn Reson Med. 2010, Jun;63(6):1659-1668.                              %
% ----------------------------------------------------------------------- %
% Input argument:
%   -wfat (complex fat signal)
%   -wwater (complex water signal)
%   -matrixSize
% 
% Output argument:
%   -f1 is the fat map in percentage
%   -w1 is the water map in percentage
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [ f1, w1 ] = fatQuantification( wfat, wwater, matrixSize )


    initialf1 = abs(wfat) ./ (abs(wwater) + abs(wfat));

    maskFat = zeros(matrixSize(1), matrixSize(2), matrixSize(3));
    maskFat(find(initialf1 >= 0.5)) = 1;
    maskWater = zeros(matrixSize(1), matrixSize(2), matrixSize(3));
    maskWater(find(initialf1 < 0.5)) = 1;
    maskFat = logical(maskFat);
    maskWater = logical(maskWater);

    f1 = zeros(matrixSize(1), matrixSize(2), matrixSize(3));
    w1 = zeros(matrixSize(1), matrixSize(2), matrixSize(3));

    f1(maskFat) = abs(wfat(maskFat)) ./ abs(wwater(maskFat) + wfat(maskFat));
    w1(maskWater) = abs(wwater(maskWater)) ./ abs(wwater(maskWater) + wfat(maskWater));

    f1(maskWater) = 1 - w1(maskWater);
    w1(maskFat) = 1 - f1(maskFat);
    f1(isnan(f1)) = 0;
    w1(isnan(w1)) = 0;
    w1 = w1*100;
    f1 = f1*100;

    % Account for phase interference
    f1(find(f1 < 0)) = 0;
    f1(find(f1 > 100)) = 100;
    w1(find(w1 < 0)) = 0;
    w1(find(w1 > 100)) = 100;


end

