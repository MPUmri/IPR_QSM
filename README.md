3D IPR-QSM and modified IPR-QSM    
Copyright Véronique Fortier 2018 - MIT License 
_______________________________________________

Implementation based on:
Buch, S., Liu, S., Ye, Y., Cheng, Y. C. N., Neelavalli, J., & Haacke, E. M. (2015). Susceptibility mapping of air, bone, and calcium in the head. Magnetic resonance in medicine, 73(6), 2185-2194.

- - - - - - - - - - - - - - - - - - - - - - -

Run ‘IPR_QSM_main.m’ to perform IPR-QSM (set originalIPR to 1) or modified IPR-QSM  (set originalIPR to 0)

* IPR-QSM requires 3 in-phase echoes with bipolar readout

* Modified IPR-QSM requires 6 echoes with monopolar readout

------------------------------------------------------------------------
** NOTES **

1. Fat water separation must be performed. The 3-point Dixon technique from the ISMRM fat-water separation toolbox used in the publication is   available here: https://www.ismrm.org/workshops/FatWater12/data.htm

    For the 3-point Dixon fat/water separation (note from the ISMRM toolbox):
    A subroutine of the algorithm is implemented in c++ for efficiency. The c++ file (RG.cpp) must be compiled before MATLAB can use it. This can be done in MATLAB by navigating to the source code folder, then typing in the MATLAB command window:

    mex RG.cpp (For Matlab versions after 2020, uses: mex -compatibleArrayDims RG.cpp)
    
    If this doesn't work, you may have to specify a compiler by typing: mex -setup

- - - - - - - - - - - - - - -

2. Phase unwrapping must be perforned. The quality-guided unwrapping technique used in the publication is available at: https://gitlab.com/veronique_fortier/Quality_guided_unwrapping. The resulting 3D matrix should be named 'wfreqUW'.

- - - - - - - - - - - - - - -

3. Background field removal must be performed before performed. A three level background removal technique based on the LBV algorithm is recommended. The recommended function for LBV background removal is available as part of the MEDI toolbox: http://pre.weill.cornell.edu/mri/pages/qsm.html

    A subroutine of the algorithm is implemented in c++ for efficiency. The c++ file (mexMGv6.cpp) must be compiled before MATLAB can use it. This can be done in MATLAB by navigating to the source code folder, then typing in the MATLAB command window:

    mex mexMGv6.cpp (For Matlab versions after 2020, uses: mex -compatibleArrayDims mexMGv6.cpp)
    
    If this doesn't work, you may have to specify a compiler by typing: mex -setup
